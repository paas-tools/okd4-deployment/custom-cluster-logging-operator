'Build Custom Release':
  variables:
    # update this value for a new release:
    UPSTREAM_COMMIT_REF: "4ef8752de8461d92960b332fbee413311586b759" # release 5.5
    UPSTREAM_REPOSITORY: "https://github.com/openshift/cluster-logging-operator"

    # the Harbor host and project where the generated container image + Helm chart will be stored
    REGISTRY_HOST: "registry.cern.ch"
    REGISTRY_PROJECT: "paas-tools"
    # Note:
    # $HARBOR_REGISTRY_USER and $HARBOR_REGISTRY_PASSWORD are set on the group-level for `paas-tools`!

  image: quay.io/podman/stable:latest
  tags:
  - docker-privileged

  before_script:
    # Set up authentication for pushing container image
    - podman login $REGISTRY_HOST --username $HARBOR_REGISTRY_USER --password $HARBOR_REGISTRY_PASSWORD --verbose
    # Download required dependencies
    - dnf install -y git coreutils golang-sigs-k8s-kustomize helm golang-github-instrumenta-kubeval

  script:
    # Download a copy of the upstream repository
    - |
      git clone --depth 1 --no-checkout "$UPSTREAM_REPOSITORY" cluster-logging-operator && \
      pushd cluster-logging-operator && \
      git fetch origin "$UPSTREAM_COMMIT_REF" && git checkout "$UPSTREAM_COMMIT_REF"

    # Apply patches as necessary
    # Only the "access" registry is publicly available, but it seems to have the same content, see:
    # https://catalog.redhat.com/software/containers/search
    - sed -i "s|registry.redhat.io|registry.access.redhat.com|" Dockerfile

    # Extract current upstream release version
    - RELEASE_VERSION=$(sed -rn 's|var Version \= "(.+)"|\1|p' version/version.go)

    # fully-qualified name of the image that will contain the cluster-logging-operator binary
    - |
      IMAGE_NAME=${REGISTRY_HOST}/${REGISTRY_PROJECT}/cluster-logging-operator-image
      if [ "$CI_COMMIT_REF_NAME" = "master" ]; then
        IMAGE_TAG=${RELEASE_VERSION}-${CI_COMMIT_SHA}
      else
        IMAGE_TAG=${RELEASE_VERSION}-${CI_COMMIT_REF_NAME}
      fi
      CLO_IMAGE=${IMAGE_NAME}:${IMAGE_TAG}

    # Build the operator container image
    # (this command gets run internally when invoking `make deploy-image`)
    - podman image build -t "${CLO_IMAGE}" . -f Dockerfile
    # Push the image into the registry
    - podman image push "${CLO_IMAGE}"

    - popd
    # Substitute image reference placeholders
    - |
      sed -i "s|IMAGE_NAME|${IMAGE_NAME}|g" kustomization.yaml
      sed -i "s|IMAGE_TAG|${IMAGE_TAG}|g" kustomization.yaml
    # Generate the YAML manifests for installing the operator
    - kustomize build . > manifests.yaml
    # Use Harbor pull-through cache for upstream images
    - |
      sed -i 's|value: quay.io/|value: registry.cern.ch/quay.io/|g' manifests.yaml

    # Validate the manifests we just produced
    - |
      kubeval --openshift --strict --ignore-missing-schemas \
      --schema-location https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master \
      manifests.yaml

    # Build a dummy Helm chart for the manifests so we can easily deploy them with ArgoCD
    - CHART_NAME=cluster-logging-operator-chart
    - CHART_VERSION="${IMAGE_TAG}"
    # the path (without name or tag!) of the OCI artifact that will contain the cluster-logging-operator Helm chart
    - CHART_REPO=${REGISTRY_HOST}/${REGISTRY_PROJECT}
    - |
      helm create "${CHART_NAME}" && \
      rm -rf "${CHART_NAME}/templates/"* "${CHART_NAME}/values.yaml" && \
      cp manifests.yaml "${CHART_NAME}/templates/" && \
      sed -i "s|^version:.*|version: ${CHART_VERSION}|g" "${CHART_NAME}/Chart.yaml" && \
      sed -i "s|^appVersion:.*|appVersion: ${UPSTREAM_COMMIT_REF}|g" "${CHART_NAME}/Chart.yaml"
    - helm package "${CHART_NAME}"

    # Publish Helm chart as OCI artifact
    # https://helm.sh/docs/topics/registries/#the-push-subcommand
    - echo "Pushing Helm chart to oci://${CHART_REPO}/${CHART_NAME}:${CHART_VERSION}"
    - helm push "${CHART_NAME}-${CHART_VERSION}.tgz" "oci://${CHART_REPO}" --registry-config "/run/containers/$(id -u)/auth.json"

  artifacts:
    paths:
      - manifests.yaml
