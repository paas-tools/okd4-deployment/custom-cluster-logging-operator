# Custom Cluster Logging Operator

This repository contains a Gitlab CI pipeline that rebuilds the upstream [Cluster Logging Operator](https://github.com/openshift/cluster-logging-operator) because it is currently [not available as an operator on OKD](https://github.com/okd-project/okd/issues/456).
For details how this is done check the [pipeline definition](./.gitlab-ci.yml).

There are two output artifacts:

* the operator's container image: `registry.cern.ch/paas-tools/cluster-logging-operator-image:${VERSION}`
* the Helm chart for installing the operator:
  - OCI artifact: `registry.cern.ch/paas-tools/cluster-logging-operator-chart`
  - chart version: `${VERSION}` (*this is equivalent to an image tag*)
* `$VERSION` is of the form `X.Y-$REF`, where `$REF` refers to a branch name, tag or commit

**TLDR**: install the operator with:

```
helm install -n openshift-logging cluster-logging-operator --version "5.5-ea8aa1d0b1a53252700f35c549f33691d66695b2" oci://registry.cern.ch/paas-tools/cluster-logging-operator-chart
```

## Updating

To build a new release, go to the `UPSTREAM_REPOSITORY`, switch to the desired release branch (e.g. `release-5.5`) and copy the SHA of the latest commit on that branch.
Update the value of `UPSTREAM_COMMIT_REF` in the `.gitlab-ci.yml` and create a merge request.
A new image and Helm chart will be built and the `$VERSION` refers to the name of the branch.

After merging to master, the CI pipeline will build the image and publish the Helm chart according to the output artifacts described above.
